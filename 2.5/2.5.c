#include <stdio.h>

void line_of_stars(void);

main () {
	line_of_stars();
	printf("Ledger line 1\n");
	line_of_stars();
	printf("Ledger line 2\n");
	line_of_stars();
}

void line_of_stars(void) {
	printf("******************************************\n");
}
