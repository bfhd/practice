#include <stdio.h>

main () {
	int result;
	result = rename("filea", "fileatemp");
	if (result != 0) {
		perror("error renaming file");
		return 0;
	} else {
		puts("Filea successfully renamed");
	}
	rename("fileb", "filea");
	rename("fileatemp", "fileb");
}
