#include <stdio.h>

void hash_line(void);
void hash_gap(void);

main () {
	int i = 0;
	for (i=0;i<5;i++) {
		/*hash line first*/
		hash_line();
		/*annual conf line next*/
		printf("###     Annual Conference     ###\n");
		hash_line();
		/*name line*/
		printf("### Name:                     ###\n");
		hash_gap();
		hash_line();
		/*organisation line*/
		printf("### Organisation:             ###\n");
		hash_gap();
		hash_line();
	}
}

void hash_line(void) {
	printf("#################################\n");
}

void hash_gap(void) {
	printf("###                           ###\n");
}
