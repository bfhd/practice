#include <stdio.h>

void one();
void two();
void three();
void four();
void five();
void six();

main () {
	six();
}

void one() {
	printf("*");
}

void two() {
	int i;
	for (i=0;i<(2*1);i++) {
		one();
	}
}

void three() {
	int i;
	for (i=0;i<(3*2);i++) {
		two();
	}
}

void four() {
	int i;
	for (i=0;i<(4*3);i++) {
		three();
	}
}

void five() {
	int i;
	for (i=0;i<(5*4);i++) {
		four();
	}
}

void six() {
	int i;
	for (i=0;i<(6*5);i++) {
		five();
	}
}

