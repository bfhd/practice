#include <stdio.h>
#include <stdlib.h>
#include <time.h>

char days_tens, day_units, month, year;
void convert();
int exp_conv(char letter);

main () {
	char ask;
	ask = 'y';
	srand((unsigned)time(NULL));
	printf("Random expiry date converter.\n");
	printf("Press y to continue, anything else to stop.\n");
	while (ask == 'y') {
		days_tens = (char)(rand()%4) + 'A';
		day_units = (char)(rand()%10)+ 'A';
		month =  (char)(rand()%10) + 'A';
		year =  (char)(rand()%10) + 'A';
		convert();
		ask = getchar();
		getchar();
	}
}

void convert() {
	int idays_tens, iday_units, imonth, iyear;
	idays_tens = exp_conv(days_tens);
	iday_units = exp_conv(day_units);
	imonth = exp_conv(month);
	iyear = exp_conv(year);
	printf("Code: %c%c%c%c\n", days_tens, day_units, month, year);
	printf("Expiry date: %d%d/%d/199%d\n", idays_tens, iday_units, imonth, iyear);
}

int exp_conv(char letter) {
	int result = 0;
	switch (letter) {
		case 'A':
			result = 0;
			break;
		case 'B':
			result = 1;
			break;
		case 'C':
			result = 2;
			break;
		case 'D':
			result = 3;
			break;
		case 'E':
			result = 4;
			break;
		case 'F':
			result = 5;
			break;
		case 'G':
			result = 6;
			break;
		case 'H':
			result = 7;
			break;
		case 'I':
			result = 8;
			break;
		case 'J':
			result = 9;
			break;
		default:
			result = -1;
			break;
		}
	return result;
}

