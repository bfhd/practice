#include <stdio.h>

main() {
	printf("C strings can include these special escape sequences:\n");
	printf("  \\n    newline\n");
	printf("  \\\"    the quote character itself\n");
	printf("  \\\\    the backslash itself\n");
	printf("  \\0    the nul character\n");
}
