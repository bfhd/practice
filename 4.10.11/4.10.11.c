#include <stdio.h>

void print_spaces(int spaces);

main () {
	int i, j, k;
	k=0;
	for (j=4;j>0;j--) {
		print_spaces(10+k);
		printf("*\n");
		k=k+j;
	}
	k--;
	for (i=1;i<=4;i++) {
		print_spaces(10+k);
		printf("*\n");
		k=k-i;
	}
	k=0;
	for (j=4;j>0;j--) {
		print_spaces(10-k);
		printf("*\n");
		k=k+j;
	}
	k=0;
	for (i=0;i<=5;i++) {
		print_spaces(i+k);
		printf("*\n");
		k=k+i;
	}

}

void print_spaces(int spaces) {
	int i;
	for (i=0;i<spaces;i++) {
		printf(" ");
	}
}
