#include <stdio.h>

void print_line(double p1, double p2);

main () {
	double m1=1.0;
	double m2=0.5;
	int i;
	printf("M1     M2     Result\n");
	for (i = 0;i < 10;i++) {
		print_line(m1, m2);
		m1 = m1 + 1.0;
		m2 = m2 + 0.5;
	}
}

void print_line(double p1, double p2) {
	double t;
	t = (2 * 9.80665 * p1 * p1)/(p1 + p1);
	printf("%.1f    %.1f    %.1f\n",p1, p2, t);
}



