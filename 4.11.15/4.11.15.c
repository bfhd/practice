#include <stdio.h>


main () {
	double v, r;
	printf("volts         resistance\n");
	v=1;
	while (v <= 10) {
		r=v/0.5;
		printf("%.3f         %.3f\n",v,r);
		v = v + 0.5;
	}
}
