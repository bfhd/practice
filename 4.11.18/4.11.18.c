#include <stdio.h>
#include <math.h>

int fact(int num);
double crazy_fact(double num);

main () {
	int i;
	printf("number   factorial   crazy factorial\n");	
	for (i=1;i<=10;i++) {
		printf("%-7d   %-10d     %-5.3f\n", i, fact(i), crazy_fact((double)i));
	}
}

int fact(int num) {
	int i, j;
	j=num;
	for (i = 1; i < j; i++) {
		num = num * i;
	}
	return num;
}

double crazy_fact(double num) {
	num = (sqrt(2*3.1415*num)*(pow((num/2.71828),num)));
	return num;
}
