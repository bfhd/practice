#include <stdio.h>
#include <math.h>

main () {
	int i = 0, j = 0;
	printf("      Table of Square Roots of Numbers from 0-99\n");
	printf("         0      1      2      3      4      5      6      7      8      9\n");
	printf("    ====== ====== ====== ====== ====== ====== ====== ====== ====== ======\n");
	for (i=0;i<=90;i+=10) {
		printf("%2d: ",j);
		for (j=i;j<(i+10);j++) {
			printf("%.4f ",sqrt(j));
		}
		printf("\n");
	}
}
