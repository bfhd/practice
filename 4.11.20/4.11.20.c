#include <stdio.h>
#include <math.h>

main () {
	double x, y, r;
	r = 1.0; //circle of radius 1
	int i;
	for (i=0;i<=360;i+=18) {
		x = r * cos(i);
		y = r * sin(i);
		printf("%f, %f\n",x, y);
	}
}

