#include <stdio.h>
#include <math.h>


main () {
	double total, difference, fraction, end;
	total = 1;
	fraction = 0.5;
	difference = 1;
	end = 0.0001;
	while (difference > end) {
		difference = total - fraction;
		total = total + fraction;
		fraction = fraction / 2;
		printf("%f, %f, %f\n", total, fraction, difference);
	}
	printf("%f\n", total);
}


