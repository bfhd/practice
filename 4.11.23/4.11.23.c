#include <stdio.h>

main () {
	int n, count = 0;
	printf("Enter an integer: ");
	scanf("%d", &n);
	if (n==0) count++;
	while (n!=0) {
		n/=10;
		++count;
	}
	printf("Number of digits: %d\n", count);
}

