#include <stdio.h>

int F1, F2, num;
void fibs();


main () {
	printf("Enter two start numbers, separated by a space: ");
 	scanf("%d %d", &F1, &F2);
	printf("Enter amount of numbers to generate: ");
	scanf("%d", &num);
	fibs();
}

void fibs() {
	int i, total = 0, prev1, prev2;
	prev1 = F1;
	prev2 = F2;
	for (i=0;i<num;i++) {
		total = prev1 + prev2;
		printf("%d\n", total);
		prev2 = prev1;
		prev1 = total;

	}
}

