#include <stdio.h>

float D;
void print_results(void);

main () {
	printf("Enter diameter: \n");
	scanf("%f", &D);
	while (D >= 0) {
		print_results();
		printf("Enter diameter: \n");
		scanf("%f", &D);
	}
}

void print_results(void) {
	float pi = 3.1415926535, r;
	r = D / 2;
	printf("The diameter is: %f, the radius is %f, the area is %f\n",D, r/2, pi * (r * r));
}

		
