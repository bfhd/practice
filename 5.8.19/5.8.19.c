#include <stdio.h>

float process_student(float f1, float f2, float t1, float t2, float e, char *student);

main () {
	float f1, f2, t1, t2, e, total;
	int students;
	char student[80] = "no";
	students = 0;
	while (student[0] != '0') {
		printf("Enter student info: \n");
		scanf("%s %f %f %f %f %f", student, &f1, &f2, &t1, &t2, &e);
		total += process_student(f1, f2, t1, t2, e, student);
		students++;
	}
	printf("Average total = %f\n", total/students);
}

float process_student(float f1, float f2, float t1, float t2, float e, char student[80]) {
	f1 = f1 * 0.05;
	f2 = f2 * 0.05;
	t1 = t1 * 0.1;
	t2 = t2 * 0.1;
	e =  e * 0.7;
	printf("%s %f\n", student, f1+f2+t1+t2+e);
	return f1+f2+t1+t2+e;
}

