#include <stdio.h>
#include <math.h>

main () {
	float input, result;
	int terms, i;
	printf("Enter Value: \n");
	scanf("%f", &input);
	while (input > 1 || input < 0) {
		printf("Outside range, try again:\n");
		scanf("%f", &input);
	}
	printf("Enter no. of terms:\n");
	scanf("%d",&terms);
	while (terms <= 0) {
		printf("Outside range, try again:\n");
		scanf("%d", &terms);
	}
	result = input;
	for (i=0;i<terms;i++) {
		if ((i%2) == 0) {
			result += (pow(input, i+2)/(i+2));
		} else {
			result -= (pow(input, i+2)/(i+2));
		}
	}
	printf("Result = %f\n",result);
	printf("Atan: %f\n",atan(input));
}


