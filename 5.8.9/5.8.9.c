#include <stdio.h>

main () {
	int temp;
	printf("Enter F temp: ");
	scanf("%d", &temp);
	while (temp != -999) {
		printf("C temp = %d degrees.\n", (5 * (temp - 32)) / 9);
		printf("Enter F temp: ");
		scanf("%d", &temp);
	}
}
