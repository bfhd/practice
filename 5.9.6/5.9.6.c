#include <stdio.h>

void print_star(void);

main () {
	int numstars;
	printf("How many stars? ");
	scanf("%d",&numstars);
	while (numstars > 0) {
		print_star();
		numstars--;
	}
}

void print_star(void) {
	printf(" * \n");
	printf("***\n");
	printf(" * \n\n");
}

