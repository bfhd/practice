#include <stdio.h>

void print_top(void);
void print_mid(void);

main () {
	int numloops, loop;
	printf("How many stars? ");
	scanf("%d", &numloops);
	while (numloops > 8) {
		printf("Must be 8 or less.\n Try again: ");
		scanf("%d", &numloops);
	}
	loop = numloops;
	while (loop > 0) {
		print_top();
		loop--;
		printf(" ");
	}
	printf("\n");
	loop = numloops;
	while (loop > 0) {
		print_mid();
		loop--;
		printf(" ");
	}
	printf("\n");
	loop = numloops;
	while (loop > 0) {
		print_top();
		loop--;
		printf(" ");
	}
	printf("\n");
}

void print_top(void) {
	printf(" * ");
}

void print_mid(void) {
	printf("***");
}
