#include <stdio.h>

main () {
	int start, end, i;
	printf("Enter start year:\n");
	scanf("%d",&start);
	printf("Enter end year:\n");
	scanf("%d",&end);
	for (i=start; i<end; i++) {
		int date;
		date = (22+((19*(i%19)+24))%30)+(2*(i%4)+(4*(i%7))+(6*((19*(i%19)+24)))+5)%7;
		if (date > 31) {
			printf("April %d, %d\n",date - 31,i);
		} else {
			printf("March %d, %d\n",date, i);
		}
	}
}
