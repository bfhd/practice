#include <stdio.h>

int convert_r(char r[]);
int get_r(char r);


main() {
	int result;
	char roman[80];

	printf("Enter roman numerals:\n");
	roman[0] = '0';
	while (roman[0] != '#') {
		fgets(roman, 80, stdin);
		result = convert_r(roman);
		printf("result = %d\n", result);
	}
}

int convert_r(char r[]) {
	int len = 0, total = 0, i = 0;
	len = strlen(r);
	while (i < len) {
		int p1, q1;
		p1 = get_r(r[i]);
		q1 = get_r(r[i + 1]);
		if (q1 > p1) {
			total -= p1;
		}
		else {
			total += p1;
		}
		i++;
	}
	return total;
}

int get_r(char r) {
	switch(r) {
	case 'M':
		return 1000;
	case 'D':
		return 500;
	case 'C':
		return 100;
	case 'L':
		return 50;
	case 'X':
		return 10;
	case 'V':
		return 5;
	case 'I':
		return 1;
	default:
		return 0;
	}
}
