#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *encrypt_message(char message[], char tran_table[]);
char *decrypt_message(char message[], int check_table[], char tran_table[]);
char *generate_tran_table(int code,int check_table[]);
void usage_exit(char name[]);

int main(int argc, char *argv[]) {
	char string[80][80], filename[80], buffer[80], outfile[80], opt;
	int i, code,j;
	if (argc != 5) {
		usage_exit(argv[0]);
	}
	strcpy(filename,argv[1]);
	code = atoi(argv[2]); //atoi converts string to integer
	opt = *argv[3];
	strcpy(outfile, argv[4]);
	if ((opt != 'e' && opt != 'E' && opt != 'd' && opt != 'D') || code == 0) { //'e'ncrypt or 'd'ecrypt
		usage_exit(argv[0]);
	}

	/*
	printf("Enter file name:\n");
	fgets(filename, 80, stdin);
	printf("Enter code number:\n");
	scanf("%d", &code);
	*/
	/*read input seed file to array*/
	FILE *input;
	FILE *output;
	size_t ln = strlen(filename) - 1;
	if (filename[ln] == '\n')
		filename[ln] = '\0'; //remove crlf from filename :(
	input = fopen(filename, "r");

	if (input == NULL) { printf("error: %s not found\n", filename); getchar(); exit(0); }
	i = 0;
	while (fgets(buffer, 80, input)) {
		strcpy(string[i], buffer);
		i++;
	}

	/*generate translation table*/
	char tran_table[27];
	int check_table[26];
	char result[80][80];
	strcpy(tran_table, generate_tran_table(code,check_table));
	
	if (opt == 'e' || opt == 'E') {
		for (int k = 0; k < i; k++) {
			strcpy(result[k], encrypt_message(string[k], tran_table));
		}
	} else if (opt == 'd' || opt == 'D') {
		for (int k = 0; k < i; k++) {
			strcpy(result[k], decrypt_message(string[k], check_table, tran_table));
		}
		
	} else {
		usage_exit(argv[0]);
	}
	for (j = 0; j < i; j++) {
		printf("%s\n", result[j]);
	}
	/* create output file */
	output = fopen(outfile,"w");
	for(int k = 0; k < i; k++) {
		fputs(result[k],output);
		fputc('\n',output);
	}

	fclose(input);
}

char *encrypt_message(char message[], char tran_table[]) {
	int i = 0, len = 0, swapch = 0;
	len = strlen(message);
	for (i = 0; i < len; i++) {
		swapch = message[i] - 'A';
		message[i] = tran_table[swapch];
	}
	message[i] = '\0';
	return message;
}

char *generate_tran_table(int code,int check_table[]) {
	/*int check_table[26];*/
	char tran_table[27];
	srand(code);
	int rnd, i;
	rnd = rand() % 26;

	for (i = 0; i < 26; i++) {
		check_table[i] = '#';
		tran_table[i] = '\0';
	}

 
	for (i = 0; i < 26; i++) {
		while (check_table[rnd] != '#') {
			rnd = rand() % 26;
		}
		check_table[rnd] = i;
		tran_table[i] = 'A' + rnd;
		rnd = rand() % 26;
	}
	tran_table[26] = '\0';
	return &tran_table;
}

char *decrypt_message(char message[], int check_table[], char tran_table[]){
	int i = 0, len = 0, charpos = 0;
	unsigned int matchchar = 0;
	len = strlen(message);
	for (i = 0; i < (len-1); i++) {
		matchchar = message[i] - 'A';
		charpos = 0;
		// find character in check table
		while (matchchar != (tran_table[charpos] - 'A')) {
			charpos++;
		}
		message[i] = charpos + 'A'; // TODO: this line
	}
	message[i] = '\0';
	return message;
}

void usage_exit(char name[]) {
	printf("Usage: %s [input filename] [encryption code] [(e)ncrypt/(d)ecrypt] [output filename]\n(warning: outfile gets clobbered)\n", name);
	exit(0);
}
