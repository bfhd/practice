#include <stdio.h>
//#include <conio.h> // for clrscr()
#include <ncurses.h>

void print_board(char board[20][20]);
void print_board_line(char line[20]);
void print_board_bottom();
void add_plant(char board[20][20], int x, int y);
void play_game(int rounds, char board1[20][20], char board2[20][20]);
int count_neighbours(char a, char b, char c, char d, char e, char f, char g, char h);
void initialise_board(char board[20][20]);
void swap_board(char boardsource[20][20], char boarddest[20][20]);

int main() {
	char decision;
	int x, y, rounds;
	char board1[20][20], board2[20][20];
	initialise_board(board1);
	initialise_board(board2);
	print_board(board1);
	printf("(S)tart ,(E)nter new plant, or (Q)uit?\n");
	decision = getchar();
	while (decision != 'Q' && decision != 'q') {
		if (decision == 'S' || decision == 's') {
			printf("How many rounds?\n");
			scanf("%d", &rounds);
			getchar(); // for the newline
			play_game(rounds, board1, board2);
		}
		else if (decision == 'E' || decision == 'e') {
			printf("Enter plant coordinates in the format x,y:\n");
			scanf("%d,%d", &x, &y);
			getchar(); // for the newline
			add_plant(board1, x-1, y-1);
			print_board(board1);
		}
		else {
			printf("Invalid choice - try again!\n");
			
		}
		printf("(S)tart, (E)nter new plant, or (Q)uit?\n");
		decision = getchar();
		getchar(); // for the newline
	}
	printf("Thanks for playing!\n");
}


void print_board(char board[20][20]) {
	//clrscr(); conio.h no longer exists ;(
	clear(); //only curses
	printf("Conway's Game of Life - by Ben!\n\n");
	for (int i = 0; i < 20; i++) {
		printf("%2d", i + 1);
		print_board_line(board[i]);
	}
	print_board_bottom();
}

void print_board_line(char line[20]) {
	for (int i = 0; i < 20; i++) {
		printf("%c", line[i]);
	}
	printf("\n");
}

void print_board_bottom() {
	printf("  --------------------\n");
	printf("  1|3|5|7|9|1|3|5|7|9|\n");
	printf("   2 4 6 8 0 2 4 6 8 0\n");
}

void add_plant(char board[20][20], int x, int y) {
	board[x][y] = '*';
}

void play_game(int rounds, char board1[20][20], char board2[20][20]) {
	int i, j, neighbours = 0;
	while (rounds != 0) {
		for (i = 0; i < 20; i++) {
			for (j = 0; j < 20; j++) {
					if (j == 0) { //top row
						neighbours = count_neighbours(
							'.',
							'.',
							board1[i][j - 1],
							board1[i][j + 1],
							board1[i + 1][j],
							board1[i + 1][j - 1],
							'.',
							board1[i + 1][j + 1]
							);
					}
					else if (j == 20) { //bottom row
						neighbours = count_neighbours(
							board1[i - 1][j - 1],
							board1[i - 1][j],
							board1[i][j - 1],
							board1[i][j + 1],
							'.',
							'.',
							board1[i - 1][j + 1],
							'.'
							);
					}
					else if (i == 0) { //left-most row
						neighbours = count_neighbours(
							'.',
							board1[i - 1][j],
							'.',
							board1[i][j + 1],
							board1[i + 1][j],
							'.',
							board1[i - 1][j + 1],
							board1[i + 1][j + 1]
							);
					}
					else if (i == 20) { //right-most row
						neighbours = count_neighbours(
							board1[i - 1][j - 1],
							board1[i - 1][j],
							board1[i][j - 1],
							'.',
							board1[i + 1][j],
							board1[i + 1][j - 1],
							'.',
							'.'
							);
					} else if (i == 0 && j == 0) { //top left corner
						neighbours = count_neighbours(
							'.',
							'.',
							'.',
							board1[i][j + 1],
							board1[i + 1][j],
							'.',
							'.',
							board1[i + 1][j + 1]
							);
					} else if (j == 20 && i == 0) { //top right corner
						neighbours = count_neighbours(
							'.',
							board1[i - 1][j],
							'.',
							board1[i][j + 1],
							'.',
							'.',
							board1[i - 1][j + 1],
							'.'
							);
					}
					else if (j == 0 & i == 20) { //bottom left corner
						neighbours = count_neighbours(
							'.',
							'.',
							board1[i][j - 1],
							'.',
							board1[i + 1][j],
							board1[i + 1][j - 1],
							'.',
							'.'
							);
					}
					else if (i == 20 && j == 20) { //bottom right corner
						neighbours = count_neighbours(
							board1[i - 1][j - 1],
							board1[i - 1][j],
							board1[i][j - 1],
							'.',
							'.',
							'.',
							'.',
							'.'
							);
					}
					else { //everywhere else
						neighbours = count_neighbours(
							board1[i - 1][j - 1],
							board1[i - 1][j],
							board1[i][j - 1],
							board1[i][j + 1],
							board1[i + 1][j],
							board1[i + 1][j - 1],
							board1[i - 1][j + 1],
							board1[i + 1][j + 1]
							);
					}
					if (board1[i][j] == '*') { //alive
						if (neighbours == 2 || neighbours == 3) {
							board2[i][j] = '*';
						}	
					} else if (board1[i][j] == '.') { //
						if (neighbours == 3) {
							board2[i][j] = '*'; //new growth
						}
					} else {
						board2[i][j] = '.';
					}
					neighbours = 0;
				}

		}
		rounds--;
		print_board(board2);
		getchar();
		swap_board(board2, board1);
		initialise_board(board2);
	}
}

int count_neighbours(char a, char b, char c, char d, char e, char f, char g, char h) {
	int total = 0;
	if (a == '*') total++;
	if (b == '*') total++;
	if (c == '*') total++;
	if (d == '*') total++;
	if (e == '*') total++;
	if (f == '*') total++;
	if (g == '*') total++;
	if (h == '*') total++;
	return total;
}

void swap_board(char boardsource[20][20], char boarddest[20][20]) {
	int i = 0, j = 0;
	for (i = 0; i < 20; i++) {
		for (j = 0; j < 20; j++) {
			boarddest[i][j] = boardsource[i][j];
		}
	}
}

void initialise_board(char board[20][20]) {
	int i = 0, j = 0;
	for (i = 0; i < 20; i++) {
		for (j = 0; j < 20; j++) {
			board[i][j] = '.';
		}
	}
	//board[20][20] = 
	//	{ '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.' },
	//	{ '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.' },
	//	{ '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.' },
	//	{ '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.' },
	//	{ '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.' },
	//	{ '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.' },
	//	{ '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.' },
	//	{ '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.' },
	//	{ '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.' },
	//	{ '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.' },
	//	{ '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.' },
	//	{ '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.' },
	//	{ '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.' },
	//	{ '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.' },
	//	{ '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.' },
	//	{ '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.' },
	//	{ '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.' },
	//	{ '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.' },
	//	{ '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.' },
	//	{ '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.', '.' }
	//;
}
