﻿using System;
using DataAccess;

namespace csvtest
{
    class Program
    {
        static void Main(string[] args)
        {
            DataTable dt = DataTable.New.ReadCsv(@"c:\temp\Book1.txt");

            foreach (Row row in dt.Rows) {
                Console.WriteLine(row["first name"]);
                Console.WriteLine(row["mobile"]);
                Console.WriteLine(row["city"]);
                Console.ReadLine();
            }
        }
    }
}
