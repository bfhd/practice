﻿using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using pimport.Corridor;

namespace pimport
{
    public class AuthenticationClientMessageInspector : IAuthenticationClientMessageInspector
    {

        #region Properties

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the password hash.
        /// </summary>
        /// <value>
        /// The password hash.
        /// </value>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the client hostname.
        /// </summary>
        /// <value>
        /// The client hostname.
        /// </value>
        public string ClientHostName { get; set; }

        #endregion

        #region IClientMessageInspector Methods

        public void AfterReceiveReply(ref Message reply, object correlationState)
        {
        }

        public object BeforeSendRequest(ref Message request, System.ServiceModel.IClientChannel channel)
        {
            var usernameHeader = MessageHeader.CreateHeader("UserName", "", UserName);

            request.Headers.Add(usernameHeader);

            var passwordHeader = MessageHeader.CreateHeader("Password", "", Password);

            request.Headers.Add(passwordHeader);

            var clientHostNameHeader = MessageHeader.CreateHeader("ClientHostName", "", ClientHostName);
            request.Headers.Add(clientHostNameHeader);

            return null;
        }

        #endregion

        #region IEndpointBehavior

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, System.ServiceModel.Dispatcher.ClientRuntime clientRuntime)
        {
            // Add this instance as a message inspector
            clientRuntime.MessageInspectors.Add(this);
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, System.ServiceModel.Dispatcher.EndpointDispatcher endpointDispatcher)
        {
        }

        public void Validate(ServiceEndpoint endpoint)
        {
        }
        #endregion
    }
}
