﻿using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using pimport.Corridor;

namespace pimport
{
    public interface IAuthenticationClientMessageInspector : IClientMessageInspector, IEndpointBehavior
    {
        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        string UserName { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        string Password { get; set; }

        /// <summary>
        /// Gets or sets the client hostname.
        /// </summary>
        /// <value>
        /// The client hostname.
        /// </value>
        string ClientHostName { get; set; }
    }
}