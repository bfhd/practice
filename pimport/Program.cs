﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using pimport.Corridor;
using System.Diagnostics;

namespace pimport
{
    class Program
    {
        public static void Main(string[] args)
        {
            if (args.Length != 3)
            {
                UsagePrint();
                Environment.Exit(0);
            }
            
            DataTable dt = DataTable.New.ReadCsv(args[0]);
            Subject patient = new Subject();
            Author author = new Author();
            if (args[1] == "P") //Patient
            {
                //patient object?
                foreach (Row row in dt.Rows)
                {
                    //insert subject table stuff into object here?
                    //add via wsdl too
                    //log to console and log file?
                    patient.SubjectId = row["SubjectId"];
                    patient.SiteCode = row["SiteCode"];
                    patient.FirstName = row["FirstName"];
                    patient.MiddleName = row["MiddleName"];
                    patient.LastName = row["LastName"];
                    patient.DateofBirth = Convert.ToDateTime(row["DateofBirth"]);
                    patient.SocialSecurityNumber = row["SocialSecurityNumber"];
                    patient.Sex = row["Sex"];
                    patient.ExternaId = row["ExternalId"];
                    patient.LastUpdate = DateTime.Now;
                    patient.UpdatedBy = row["UpdatedBy"]; // this will be done by corridor anyway
                    patient.HL7MessageType = row["HL7MessageType"];
                    patient.ServiceCode = row["ServiceCode"];
                    patient.PreferredLanguage = row["PreferredLanguage"];
                    patient.Race = row["Race"];
                    patient.Ethnicity = row["Ethnicity"];
                    patient.SmokingStatus = row["SmokingStatus"];
                    patient.Title = row["Title"];
                    patient.IsDeceased = Convert.ToBoolean(row["IsDeceased"]);
                    patient.TimeOfDeath = Convert.ToDateTime(row["TimeOfDeath"]);
                    patient.IsSurrogate = Convert.ToBoolean(row["IsSurrogate"]);
                    patient.GeneralPractitioner = row["GeneralPractitioner"];
                    patient.Uid = row["Uid"];
                    patient.AutoCC = row["AutoCC"];
                }
                
            }
            else if (args[1] == "D") //doctor
            {
                //doctor object
                foreach (Row row in dt.Rows)
                {
                    //author table
                    //wsdl
                    //log output
                    author.Active = row["Active"];
                    author.AdditionalCopies = row["AdditionalCopies"];
                    author.AuthorCode = row["AuthorCode"];
                    author.DifficultyFactor = row["DifficultyFactor"];
                    author.DistributionMethod = row["DistributionMethod"];
                    author.Esig = row["Esig"];
                    author.EsigPriority = row["EsigPriority"];
                    author.ExternalID = row["ExternalId"];
                    author.LastUpdated = DateTime.Now;
                    author.FirstName = row["FirstName"];
                    author.MiddleName = row["MiddleName"];
                    author.LastName = row["LastName"];
                    author.LicenseNumber = row["LicenseNumber"];
                    author.Prefix = row["Prefix"];
                    author.Suffix = row["Suffix"];
                    author.ServiceCode = row["ServiceCode"];
                    author.SiteCode = row["SiteCode"];
                    author.Specialty = row["Specialty"];
                    author.Title = row["Title"];
                }
            }
            else //what are you doing
            {
                Console.WriteLine("You need to use \"P\" for patient import and \"D\" for doctor import");
                UsagePrint();
                Environment.Exit(0);
            }
        }
        
        public static void UsagePrint()
        {
            Console.WriteLine("USAGE: pimport.exe [filepath\filename] [\"P\"atient or \"D\"octor] [WSDL url]");
        }

        public class Subject
        {
            public string SubjectId;
            public string SiteCode;
            public string FirstName;
            public string MiddleName;
            public string LastName;
            public DateTime DateofBirth;
            public string SocialSecurityNumber;
            public string Sex;
            public string ExternaId;
            public DateTime LastUpdate;
            public string UpdatedBy;
            public string HL7MessageType;
            public string ServiceCode;
            public string PreferredLanguage;
            public string Race;
            public string Ethnicity;
            public string SmokingStatus;
            public string Title;
            public bool IsDeceased;
            public DateTime TimeOfDeath;
            public bool IsSurrogate;
            public string GeneralPractitioner;
            public string Uid;
            public string AutoCC;
        }

        public class Author
        {
            public string AuthorCode;
            public string SiteCode;
            public string FirstName;
            public string MiddleName;
            public string LastName;
            public string Specialty;
            public string Title;
            public string Esig;
            public string DifficultyFactor;
            public string DistributionMethod;
            public string AdditionalCopies;
            public string ExcludeTypes;
            public string ExcludeTypesBatchPrint;
            public string ExcludeTypesBatchFax;
            public string ExcludeTypesEmail;
            public string ExternalID;
            public DateTime LastUpdated;
            public string UpdatedBy;
            public string LicenseNumber;
            public string HL7MessageType;
            public string Active;
            public string ServiceCode;
            public string Prefix;
            public string Suffix;
            public string EsigPriority;
            public string Uid;
        }

        public class Address
        {
            public int AddressTypeId;
            public string LicenseNumber;
            public string AddressLine1;
            public string AddressLine2;
            public string AddressLine3;
            public string AddressLine4;
            public string City;
            public string State;
            public string Zip;
            public string PhoneNumber;
            public string PhoneExtension;
            public string FaxNumber;
            public bool DistributionType;
            public bool AddressClass;
            public bool IsLocalAddress;
            public string ServiceCode;
            public string HomePhone;
            public string SiteCode;
            public string Email;
            public string Country;
            public string PracticeCode;
            public string DistributionTypeId;
        }

    }
    
}
/*Address:
[AddressTypeId]
[LicenseNumber]
[AddressLine1]
[AddressLine2]
[AddressLine3]
[AddressLine4]
[City]
[State]
[Zip]
[PhoneNumber]
[PhoneExtension]
[FaxNumber]
[DistributionType]
[AddressClass]
[IsLocalAddress]
[ServiceCode]
[HomePhone]
[SiteCode]
[Email]
[Country]
[PracticeCode]
[DistributionTypeId]

Author:
[AuthorCode]
[SiteCode]
[FirstName]
[MiddleName]
[LastName]
[Specialty]
[Title]
[Esig]
[DifficultyFactor]
[DistributionMethod]
[AdditionalCopies]
[ExcludeTypes]
[ExcludeTypesBatchPrint]
[ExcludeTypesBatchFax]
[ExcludeTypesEmail]
[ExternalID]
[LastUpdated]
[UpdatedBy]
[LicenseNumber]
[HL7MessageType]
[Active]
[ServiceCode]
[Prefix]
[Suffix]
[EsigPriority]
[Uid]

Subject:
[SubjectId]
[SiteCode]
[FirstName]
[MiddleName]
[LastName]
[DateofBirth]
[SocialSecurityNumber]
[Sex]
[ExternalID]
[LastUpdate]
[UpdatedBy]
[HL7MessageType]
[ServiceCode]
[PreferredLanguage]
[Race]
[Ethnicity]
[SmokingStatus]
[Title]
[IsDeceased]
[TimeOfDeath]
[IsSurrogate]
[GeneralPractitioner]
[Uid]
[AutoCC]

*/