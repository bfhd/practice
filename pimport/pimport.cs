﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using pimport.Corridor;
using System.Diagnostics;
using DataAccess;

namespace pimport
{
    public class pimport
    {
        private CorridorDataServiceClient _corridorDataClient;

        /// <summary>
        /// Corridor data Service.
        /// </summary>
        private ICorridorDataService CorridorDataService
        {
            get { return (ICorridorDataService)_corridorDataClient; }
        }

        /// <summary>
        /// The underlying WCF file service client
        /// </summary>
        private IAuthenticationClientMessageInspector _authenticationClientMessageInspector;

        /// <summary>
        /// Constructor. Add authentication headers.
        /// </summary>
        public void pimporter() {
            try
            {
                string username = Convert.ToString(ConfigurationSettings.AppSettings["Username"]);
                string password = Convert.ToString(ConfigurationSettings.AppSettings["Password"]);
                string hostName = Convert.ToString(ConfigurationSettings.AppSettings["HostName"]);
                _authenticationClientMessageInspector = new AuthenticationClientMessageInspector
                {
                    UserName = username,
                    Password = password,
                    ClientHostName = hostName
                };
                _corridorDataClient = new CorridorDataServiceClient("BasicHttpBinding_ICorridorDataService");
                _corridorDataClient.ChannelFactory.Endpoint.Behaviors.Add(_authenticationClientMessageInspector);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        
        }
    }
}
